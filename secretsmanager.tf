resource "aws_iam_role_policy" "secrets_manager_readonly" {
  name   = "secrets_manager_readonly"
  count  = var.secretsmanager_readonly && var.enabled ? 1 : 0
  role   = aws_iam_role.default_role[0].id
  policy = data.aws_iam_policy_document.secretsmanager_readonly[0].json

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_iam_policy_document" "secretsmanager_readonly" {
  count = var.secretsmanager_readonly && var.enabled ? 1 : 0

  statement {
    actions = [
      "secretsmanager:Get*",
      "secretsmanager:DescribeSecret",
      "secretsmanager:List*",
    ]
    effect    = "Allow"
    resources = var.secretsmanager_arns
  }
}

resource "aws_iam_role_policy" "secretsmanager_write" {
  name   = "secretsmanager_write"
  count  = var.secretsmanager_write && var.enabled ? 1 : 0
  role   = aws_iam_role.default_role[0].id
  policy = data.aws_iam_policy_document.secretsmanager_write[0].json

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_iam_policy_document" "secretsmanager_write" {
  count = var.secretsmanager_write && var.enabled ? 1 : 0

  statement {
    actions = [
      "secretsmanager:UntagResource",
      "secretsmanager:PutSecretValue",
      "secretsmanager:CreateSecret",
      "secretsmanager:DeleteSecret",
      "secretsmanager:CancelRotateSecret",
      "secretsmanager:UpdateSecret",
      "secretsmanager:StopReplicationToReplica",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:RestoreSecret",
      "secretsmanager:RotateSecret",
      "secretsmanager:UpdateSecretVersionStage",
      "secretsmanager:RemoveRegionsFromReplication",
      "secretsmanager:ListSecrets",
      "secretsmanager:TagResource"
    ]
    effect    = "Allow"
    resources = var.secretsmanager_arns
  }
}
